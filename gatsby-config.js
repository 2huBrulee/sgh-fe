module.exports = {
  siteMetadata: {
    title: 'Horarios',
    language: 'es',
  },
  plugins: ['gatsby-plugin-react-helmet', 'gatsby-plugin-sass'],
};
