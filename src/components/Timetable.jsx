import React from 'react';
import './Timetable.css';

const columns = [
  {
    id: 1,
    nombre: 'lunes',
  },
  {
    id: 2,
    nombre: 'martes',
  },
  {
    id: 3,
    nombre: 'miercoles',
  },
  {
    id: 4,
    nombre: 'jueves',
  },
  {
    id: 5,
    nombre: 'viernes',
  },
  {
    id: 6,
    nombre: 'sabado',
  },
  {
    id: 7,
    nombre: 'domingo',
  },
];

const rows = [
  {
    id: 1,
    nombre: '8:00 - 8:50',
  },
  {
    id: 2,
    nombre: '8:50 - 9:40',
  },
  {
    id: 3,
    nombre: '9:40 - 10:30',
  },
  {
    id: 4,
    nombre: '10:30 - 11:20',
  },
  {
    id: 5,
    nombre: '11:20 - 12:10',
  },
  {
    id: 6,
    nombre: '12:10 - 13:00',
  },
  {
    id: 7,
    nombre: '13:00 - 13:50',
  },
];

const Timetable = () => (
  <div className="timetable">
    <table>
      <thead>
        <th>
          <div className="edge" />
        </th>
        {columns.map(n => (
          <th key={n.id}>
            <div className="columns">{n.nombre}</div>
          </th>
        ))}
      </thead>
      <tbody>
        {rows.map(n => (
          <tr>
            <td key={2 * n}>
              <div className="rows">{n.nombre}</div>
            </td>
            {columns.map(m => (
              <td key={m * 100 + n}>
                <div className="slots">VACIO</div>
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

export default Timetable;
