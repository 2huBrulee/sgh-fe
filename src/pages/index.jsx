import { Link } from 'gatsby';
import React from 'react';
import Container from '../components/Container';
import Layout from '../components/Layout';
import Timetable from '../components/Timetable';

const IndexPage = () => (
  <Layout>
    <Container>
      <h1>HORARIOS</h1>
      <p>Vista por Cursos</p>
      <p>Ciclo I - Ingenieria Mecanica</p>
      <Timetable />
      <Link to="/page-2/">Segunda vista</Link>
    </Container>
  </Layout>
);

export default IndexPage;
